<?php
/*+**********************************************************************************
 * The contents of this file are subject to the vtiger CRM Public License Version 1.0
 * ("License"); You may not use this file except in compliance with the License
 * The Original Code is:  vtiger CRM Open Source
 * The Initial Developer of the Original Code is vtiger.
 * Portions created by vtiger are Copyright (C) vtiger.
 * All Rights Reserved.
 ************************************************************************************/
$languageStrings = Array(
'LBL_MODULE_NAME'=>'装运',
'LBL_SO_MODULE_NAME'=>'装运',
'LBL_RELATED_PRODUCTS'=>'产品细节',
'LBL_MODULE_TITLE'=>'装运: Home',
'LBL_SEARCH_FORM_TITLE'=>'装运查找',
'LBL_LIST_FORM_TITLE'=>'装运列表',
'LBL_LIST_SO_FORM_TITLE'=>'销售订单列表',
'LBL_NEW_FORM_TITLE'=>'新建装运',
'LBL_NEW_FORM_SO_TITLE'=>'新建销售订单',
'LBL_MEMBER_ORG_FORM_TITLE'=>'Member Organizations',

'LBL_LIST_ACCOUNT_NAME'=>'客户名称',
'LBL_LIST_CITY'=>'市/县',
'LBL_LIST_WEBSITE'=>'网站',
'LBL_LIST_STATE'=>'省/州',
'LBL_LIST_PHONE'=>'电话',
'LBL_LIST_EMAIL_ADDRESS'=>'Email',
'LBL_LIST_CONTACT_NAME'=>'联系人姓名',

//DON'T CONVERT THESE THEY ARE MAPPINGS
'db_name' => 'LBL_LIST_ACCOUNT_NAME',
'db_website' => 'LBL_LIST_WEBSITE',
'db_billing_address_city' => 'LBL_LIST_CITY',

//END DON'T CONVERT

'LBL_ACCOUNT'=>'客户:',
'LBL_ACCOUNT_NAME'=>'客户名称:',
'LBL_PHONE'=>'电话:',
'LBL_WEBSITE'=>'网站:',
'LBL_FAX'=>'传真:',
'LBL_TICKER_SYMBOL'=>'Ticker Symbol:',
'LBL_OTHER_PHONE'=>'Other Phone:',
'LBL_ANY_PHONE'=>'Any Phone:',
'LBL_MEMBER_OF'=>'Member of:',
'LBL_EMAIL'=>'Email:',
'LBL_EMPLOYEES'=>'Employees:',
'LBL_OTHER_EMAIL_ADDRESS'=>'Other Email:',
'LBL_ANY_EMAIL'=>'Any Email:',
'LBL_OWNERSHIP'=>'Ownership:',
'LBL_RATING'=>'Rating:',
'LBL_INDUSTRY'=>'Industry:',
'LBL_SIC_CODE'=>'SIC Code:',
'LBL_TYPE'=>'Type:',
'LBL_ANNUAL_REVENUE'=>'Annual Revenue:',
'LBL_ADDRESS_INFORMATION'=>'Address Information',
'LBL_Quote_INFORMATION'=>'Account Information',
'LBL_CUSTOM_INFORMATION'=>'Custom Information',
'LBL_BILLING_ADDRESS'=>'Billing Address:',
'LBL_SHIPPING_ADDRESS'=>'Shipping Address:',
'LBL_ANY_ADDRESS'=>'Any Address:',
'LBL_CITY'=>'City:',
'LBL_STATE'=>'State:',
'LBL_POSTAL_CODE'=>'Postal Code:',
'LBL_COUNTRY'=>'Country:',
'LBL_DESCRIPTION_INFORMATION'=>'Description Information',
'LBL_DESCRIPTION'=>'描述:',
'LBL_TERMS_INFORMATION'=>'条款及细则',
'NTC_COPY_BILLING_ADDRESS'=>'拷贝账单地址到装运地址',
'NTC_COPY_SHIPPING_ADDRESS'=>'Copy shipping address to billing address',
'NTC_REMOVE_MEMBER_ORG_CONFIRMATION'=>'Are you sure you want to remove this record as a member organization?',
'LBL_DUPLICATE'=>'Potential Duplicate Accounts',
'MSG_DUPLICATE' => 'Creating this vtiger_account may vtiger_potentialy create a duplicate vtiger_account. You may either select an vtiger_account from the list below or you may click on Create New Account to continue creating a new vtiger_account with the previously entered data.',

'LBL_INVITEE'=>'联系人',
'ERR_DELETE_RECORD'=>"A record number must be specified to delete the vtiger_account.",

'LBL_SELECT_ACCOUNT'=>'选择客户',
'LBL_GENERAL_INFORMATION'=>'General Information',

//for v4 release added
'LBL_NEW_POTENTIAL'=>'新建机会',
'LBL_POTENTIAL_TITLE'=>'机会',

'LBL_NEW_TASK'=>'新建任务',
'LBL_TASK_TITLE'=>'任务',
'LBL_NEW_CALL'=>'New Call',
'LBL_CALL_TITLE'=>'Calls',
'LBL_NEW_MEETING'=>'New Meeting',
'LBL_MEETING_TITLE'=>'Meetings',
'LBL_NEW_EMAIL'=>'New Email',
'LBL_EMAIL_TITLE'=>'Emails',
'LBL_NEW_CONTACT'=>'New Contact',
'LBL_CONTACT_TITLE'=>'Contacts',

//Added vtiger_fields after RC1 - Release
'LBL_ALL'=>'All',
'LBL_PROSPECT'=>'Prospect',
'LBL_INVESTOR'=>'Investor',
'LBL_RESELLER'=>'Reseller',
'LBL_PARTNER'=>'Partner',

// Added for 4GA
'LBL_TOOL_FORM_TITLE'=>'Account Tools',
//Added for 4GA
'Subject'=>'主题',
'Quote Name'=>'报价主题',
'Vendor Name'=>'Vendor Name',
'Consignment Terms'=>'装运协议',
'Contact Name'=>'Contact Name',//to include contact name vtiger_field in Consignment
'Consignment Date'=>'装运日期',
'Sub Total'=>'Sub Total',
'Due date'=>'预计日期',
'Carrier'=>'Carrier',
'Type'=>'Type',
'Sales Tax'=>'Sales Tax',
'Sales Commission'=>'Sales Commission',
'Excise Duty'=>'Excise Duty',
'Total'=>'Total',
'Product Name'=>'Product Name',
'Assigned To'=>'Assigned To',


	
'Billing Address' => '账单地址',
'Shipping Address' => '收货地址',
'Billing Address' => '账单地址',
'Billing City' => '市/县',
'Billing Code' => '邮政编码',
'Billing Country' => '国家',
'Billing Po Box' => '邮政信箱',
'Billing State' => '省/州',
'Shipping City' => '市/县',
'Shipping State' => '省/州',
'Shipping Code' => '邮政编码',
'Shipping Country' => '国家',
'Shipping Po Box' => '邮政信箱',
'City'=>'市/县',
'State'=>'省/州',
'Code'=>'邮政编码',
'Country'=>'国家',
'Created Time'=>'创建时间',
'Modified Time'=>'修改时间',
'Description'=>'Description',
'Potential Name'=>'机会名称',
'Customer No'=>'Customer No',
'Sales Order'=>'销售订单',
'Pending'=>'Pending',
'Account Name'=>'客户名称',
'Terms & Conditions'=>'条款及细则',
//Quote Info
'LBL_CONSIGNMENT_INFORMATION'=>'装运信息',
'LBL_CONSIGNMENT'=>'装运:',
'LBL_SO_INFORMATION'=>'销售订单信息',
'LBL_SO'=>'销售订单:',

//Added in release 4.2
'LBL_SUBJECT'=>'主题:',
'LBL_SALES_ORDER'=>'销售订单:',
'Consignment Id'=>'装运 Id',
'LBL_MY_TOP_CONSIGNMENT'=>'My Top Open 装运',
'LBL_CONSIGNMENT_NAME'=>'装运 Name:',
'Purchase Order'=>'购买订单',
'Status'=>'状态',
'Id'=>'装运 Id',
'Consignment'=>'装运',

//Added for existing Picklist Entries

'Created'=>'Created',
'Done'=>'Done',
//Added to Custom Consignment Number
'Consignment No'=>'装运 No',
'Adjustment'=>'调整',

//Added for Reports (5.0.4)
'Tax Type'=>'Tax Type',
'Discount Percent'=>'Discount Percent',
'Discount Amount'=>'Discount Amount',
'Terms & Conditions'=>'条款及细则',
'No'=>'No',
'Date'=>'Date',

// Added affter 5.0.4 GA
//Added for Documents module
'Documents'=>'Documents',

'SINGLE_Consignment'=>'装运',
    
//SalesPaltform.ru begin
'LBL_GOODS_CONSIGNMENT' => 'Goods consigment',    
//SalesPlatform.ru end   
);
