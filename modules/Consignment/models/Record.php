<?php
/*+**********************************************************************************
 * The contents of this file are subject to the vtiger CRM Public License Version 1.1
 * ("License"); You may not use this file except in compliance with the License
 * The Original Code is: SalesPlatform Ltd
 * The Initial Developer of the Original Code is SalesPlatform Ltd.
 * All Rights Reserved.
 * If you have any questions or comments, please email: devel@salesplatform.ru
 ************************************************************************************/

/**
 * Inventory Record Model Class
 */
class Consignment_Record_Model extends Inventory_Record_Model {
	
	// 得到创建购买订单的 链接
	// public function getCreatePurchaseOrderUrl() {
		// $purchaseOrderModuleModel = Vtiger_Module_Model::getInstance('PurchaseOrder');
		// return "index.php?module=".$purchaseOrderModuleModel->getName()."&view=".$purchaseOrderModuleModel->getEditViewName()."&invoice_id=".$this->getId();
	// }	

}

